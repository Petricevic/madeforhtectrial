﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HTECQATest;

namespace SandboxTests
{
    [TestClass]
    public class OtherFuncionalities
    {
        [TestInitialize]
        public void OpenUseCasesMenu()
        {
            PageObject.UseCasesPage.OpenUseCaseMenu();
        }

        [TestMethod]
        public void AddNewStep()
        {
            PageObject.UseCasesPage.AddNewStep();
            Assert.IsTrue(PageObject.UseCasesPage.IsStepAdded());
        }

        [TestCleanup]
        public void ReturnToUseCases()
        {
            PageObject.UseCasesPage.ReturnToUseCases(InitializeSandbox.baseUrl);
        }
    }
}
